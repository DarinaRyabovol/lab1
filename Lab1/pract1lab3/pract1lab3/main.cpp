#include <stdio.h>
#include <conio.h>

float transfer(float, char*);

int main()
{
	float mean;
	char whatIs;
	printf("enter please\n");
	scanf("%f%c",&mean,&whatIs);
	mean = transfer(mean,&whatIs);
	printf(" ~ %f%c\n", mean, whatIs);
	return 0;
}

float transfer(float tmp, char* whatIs)
{
	if (*whatIs == 'D')
	{
		*whatIs = 'R';
		return tmp*3.14/180;
	}
	else
	{
		*whatIs = 'D';
		return tmp*180/3.14;
	}
}
#include <stdio.h>
#include <stdlib.h>
#define MAXLN 5
int pow10(int step)
{
	if (step == 0)
		return 1;
	return 10*pow10(step-1);
}

int isNumber(char a)
{
	char *mystring = "0123456789";
	int i = 0;
	while((mystring[i] != a)&&(i < 10))
		i++;
	if(i == 10)
		return -1;
	return i;
}

int currentSum(int *num, int lenght)
{
	int tmpNum = 0;
	int j;
	for(j = 0; j < lenght; j++)
	{
		tmpNum += num[j]*pow10(lenght - j - 1);
	}
	return tmpNum;
}

int main()
{
	int sum = 0;
	int lenght = 0;
	int i = 0;
	int current[MAXLN];
	for(i = 0; i < MAXLN; i++)
	{
		current[i] = 0;
	}
	char tmp;
	i = 0;
	char sign = '+';
	while ((tmp = getchar()) != '\n')
	{
		int res = isNumber(tmp);
		if(res == -1)
		{
			if(lenght > 0)
			{
				sum += currentSum(current, lenght);
			}
			lenght = 0;
			i = 0;
		}
		else
		{
			if(lenght == MAXLN)
			{
				sum += currentSum(current, lenght);
				i = 0;
				lenght = 0;
			}
			current[i] = res;
			i++;
			lenght ++;
		}
	}
	if(lenght > 0)
	{
		sum += currentSum(current, lenght);
		i = 0;
		lenght = 0;
	}
	printf("\nsumm = %d\n", sum);
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

#define TREE struct tree

TREE
{
	char *mean;
	int count;
	TREE *left;
	TREE *right;
};

TREE * addTo( TREE *top, char *info)
{
	if (top == NULL)
	{
		top = (TREE*)malloc(sizeof(TREE));
		top->count = 0;
		top->mean = info;
		top->left = top->right = NULL;
		return top;
	}
	else
	{
		if(strcmp(top->mean,info) == -1)
		{
			if(top->right == NULL)
				top->right = addTo(top->right,info);
			else
				addTo(top->right,info);
		}
		if(strcmp(top->mean,info) == 1)
		{
			if(top->left == NULL)
				top->left = addTo(top->left,info);
			else
				addTo(top->left,info);
		}
		if(strcmp(top->mean,info) == 0)
			top->count ++;
	}
}

int compare(const char * str1, const char * str2)
{
	int n;
	int isBig = 0;
	if (strlen(str1) > strlen(str2))
	{
		n = strlen(str2);
		isBig = 1;
	}
	else
	{
		n = strlen(str1);
		if (strlen(str1) < strlen(str2))
			isBig = 2;
	}
	int i = 0;
	while (i < n)
	{
		if(str1[i] > str2[i])
			return 1;
		if(str2[i] > str1[i])
			return -1;
		i++;
	}
	switch(isBig)
	{
	case 0:
		return 0;
		break;
	case 1:
		return 1;
		break;
	case 2:
		return -1;
		break;
	}
}


void seach(TREE * top, char * info)
{
	if (compare(top->mean,info) == 0)
		top->count ++;
	else
	{
		if (compare(top->mean,info)==-1)
		{
			if(top->right != NULL)
				seach(top->right,info);
		}
		else
			if(top->left != NULL)
				seach(top->left,info);
	}
}

void printtree(TREE * top)
{
	if(top->left != NULL)
		printtree(top->left);
	printf("\n%s %d",top->mean, top->count);
	if(top->right != NULL)
		printtree(top->right);
}


bool isCharIn(const char *str, char a)
{
	int i = 0;
	while (i < strlen(str))
	{
		if(str[i] == a)
			return true;
		else
			i++;
	}
	return false;
}

char * copy(const char * source)
{
	char *result = (char*)malloc(sizeof(char)*(strlen(source)+1));
	int i;
	for (i = 0; i < strlen(source)+1; ++i)
	{
		result[i] = source[i];
	}
	return result;
}

void deleteTree(TREE *top)
{
	if(top->left != NULL)
		deleteTree(top->left);
	if(top->right != NULL)
		deleteTree(top->right);
	free(top);
}
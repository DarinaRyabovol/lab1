#include "tree.h"
#include <string.h>

#define chars union cha

union cha 
{
	char character;
	struct 
	{
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;

};

char createNewShortCharacter(unsigned  short *byte)
{
	chars reterned;
	reterned.byte.b1 = byte[0] - '0';
	reterned.byte.b2 = byte[1] - '0';
	reterned.byte.b3 = byte[2] - '0';
	reterned.byte.b4 = byte[3] - '0';
	reterned.byte.b5 = byte[4] - '0';
	reterned.byte.b6 = byte[5] - '0';
	reterned.byte.b7 = byte[6] - '0';
	reterned.byte.b8 = byte[7] - '0';
	return reterned.character;
}

char* seachdot(const char *str)
{
	int i = 0;
	while (str[i] != '.')
	{
		i++;
	}
	char *fileFormat = (char*)malloc(sizeof(char)*(strlen(str) - i));
	int j = 0;
	i++;
	for(i; i < strlen(str); i++)
	{
		fileFormat[j] = str[i];
		j++;
	}
	fileFormat[j] = '\0';
	return fileFormat;
}

int main()
{
	char * fileName = (char *)malloc(sizeof(char)*256);
	scanf("%s", fileName);
	FILE * file = fopen(fileName, "r");
	char tmp;
	int count = 0;
	int number = 0;
	myList *symb = NULL;
	while ((tmp = fgetc(file)) != EOF)
	{
		if(symb)
			addNode(symb, tmp, &count);
		else
			symb = addNode(symb,tmp,&count);
		number ++;
	}
	printf("\n%d %d\n", number, count);
	int i;
	Symbol * symbols = sort(createArray(symb,count),count);
	for(i = 0; i < count; i++)
	{
		symbols[i].freq /= number;
	}
	listOfNodes * root = createListOfNode(symbols, count);
	bildTree(&root);
	fclose(file);
	char start = '\0';
	createCode(root->info, &start);
	file = fopen(fileName, "r");
	FILE * tmpFile = fopen("C:\\compressor\\tmp.101", "w+");
	characters* listofCharacters = NULL;
	findInTree(root->info, &listofCharacters);
	characters * newListofCharacters = listofCharacters;
	while(newListofCharacters)
	{
		printf("\n %c %s", newListofCharacters->info,newListofCharacters->code);
		newListofCharacters = newListofCharacters->next;
	}
	while((tmp=getc(file)) != EOF)
	{
		char *currentCode = findCode(listofCharacters, tmp);
		fprintf(tmpFile,"%s",currentCode);
	}
	fseek(file,0,SEEK_END);
	long long size  = ftell(file);
	fseek(tmpFile, 0, SEEK_END);
	int tell = ftell(tmpFile)%8;
	fclose(file);
	fclose(tmpFile);
	char *dotform = seachdot(fileName);
	char sign[] = {'.','n', 'z', '\0'};
	strcat(fileName, sign);
	FILE *dest = fopen(fileName, "w+");
	tmpFile = fopen("C:\\compressor\\tmp.101", "r");
	//
	//������������ ��������������� �����
	//
	newListofCharacters = listofCharacters;
	fprintf(dest,"%s %d ", sign, count);
	for(i = 0; i < count; i++)
	{
		fprintf(dest,"%s %f ", newListofCharacters->code, newListofCharacters->freq);
		newListofCharacters = newListofCharacters->next;
	}
	fprintf(dest, "%d %ld ", tell, size);
	fprintf(dest, "%s ", dotform);
	unsigned short b[8];
	int k = 0;
	while((tmp = getc(tmpFile)) != EOF)
	{
		if(k < 8)
		{
			b[k] = tmp - '0';
			k++;
		}
		if(k == 8)
		{
			fprintf(dest,"%c",createNewShortCharacter(b));
			b[0] = tmp;
			k = 1;
		}
	}
	delete(fileName);
	fclose(tmpFile);
	fclose(dest);
	return 0;
}
#include "symbol.h"


myList * addNode(myList * root, char a, int *count)
{
	if (root)
	{
		if(root->info.name == a)
			root->info.freq +=1;
		else
			if (root->next)
				addNode(root->next,a,count);
			else
				root->next = addNode(root->next,a,count);
	}
	else
	{
		root = (myList *)malloc(sizeof(myList));
		root->next = NULL;
		root->info.freq = 1;
		root->info.name = a;
		(*count) ++;
	}
	return root;
}

Symbol * createArray(myList * root, int count)
{
	Symbol * array = (Symbol *)malloc(sizeof(Symbol)*count);
	int i = 0;
	while (root->next)
	{
		array[i] = root->info;
		myList * node = root;
		root = root->next;
		i++;
	}
	array[count - 1] = root->info;
	return array;
}

Symbol * sort(Symbol * array, int count)
{
	Symbol tmp;
	int i;
	int j;
	for (i = 0; i < count; i ++)
	{
		for (j = i + 1; j < count; j ++)
		{
			if (array[i].freq < array[j].freq)
			{
				tmp = array[i];
				array[i] = array[j];
				array[j] = tmp;
			}
		}
	}
	return array;
}

void printList(myList * root)
{
	printf("\n%c %f", root->info.name, root->info.freq);
	if(root->next)
		printList(root->next);
}

Symbol findSymbol(Symbol * arr, char a)
{
	int i = 0;
	while(arr[i].name != a)
		i++;
	return arr[i];
}
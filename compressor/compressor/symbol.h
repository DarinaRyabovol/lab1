#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
	char name;
	double freq;
} Symbol;

#define myList struct l
struct l
{
	Symbol info;
	myList * next;
};

myList * addNode(myList * root, char a, int *count);

Symbol * createArray(myList * root, int count);

Symbol * sort(Symbol * array, int count);

void printList(myList * root);

Symbol findSymbol(Symbol *, char a);
#include "tree.h"
#include <string.h>


TREE* createNode(Symbol source)
{
	TREE *tmp = (TREE *)malloc(sizeof(TREE));
	tmp->info = source;
	tmp->code = (char *)malloc(sizeof(char)*256);
	int i;
	for(i = 0; i < 256; i++)
	{
		tmp->code[i] = '\0';
	}
	tmp->left = tmp->right = NULL;
	return tmp;
}

TREE* addIn(TREE * node1, TREE * node2)
{
	TREE * tmp = (TREE*)malloc(sizeof(TREE));
	tmp->info = node1->info;
	tmp->info.freq += node2->info.freq;
	tmp->code = (char *)malloc(sizeof(char)*256);
	int i;
	for(i = 0; i < 256; i++)
	{
		tmp->code[i] = '\0';
	}
	if(node1->info.freq < node2->info.freq)
	{
		tmp->left = node1;
		tmp->right = node2;
	}
	else
	{
		tmp->right = node1;
		tmp->left = node2;
	}
	return tmp;
}

void Insert(listOfNodes * first,listOfNodes* newEl)
{
	if(first->next)
	{
		while(first->next->info->info.freq > newEl->info->info.freq)
		{
			first = first->next;
		}
		listOfNodes *tmp = first->next;
		first->next = newEl;
		first->next->next = tmp;
	}
	else
	{
		listOfNodes *a = (listOfNodes*)malloc(sizeof(listOfNodes));
		a->next = NULL;
		Symbol b = first->info->info;
		a->info = createNode(b);
		a->next = NULL;
		first->info->info.freq += newEl->info->info.freq;
		first->info->left = a->info;
		first->info->right = newEl->info;
	}
}

listOfNodes * createListOfNode(Symbol* arr, int count)
{
	listOfNodes *newList = NULL;
	int i;
	for(i = 0; i < count; i++)
	{
		listOfNodes * current = (listOfNodes*)malloc(sizeof(listOfNodes));
		current->next = NULL;
		current->info = createNode(arr[i]);
		addInEnd(&newList,current);
	}
	return newList;
}

void addInEnd(listOfNodes** root, listOfNodes * newEl)
{
	listOfNodes * tmp = *root;
	if(tmp)
	{
		while(tmp->next)
			tmp = tmp->next;
		tmp->next = newEl;
	}
	else
	{
		(*root) = newEl;
	}
}

void bildTree(listOfNodes **source)
{
	while((*source)->next)
	{
		listOfNodes *newNode = (listOfNodes *)malloc(sizeof(listOfNodes));
		newNode->next = NULL;
		listOfNodes * currentRoot = *source;
		if(!(currentRoot->next->next))
		{
			newNode->info = addIn(currentRoot->info, currentRoot->next->info);
			(*source)->next = NULL;
			Insert(*source, newNode);
		}
		else
		{
			while (currentRoot->next->next->next)
				currentRoot = currentRoot->next;
			newNode->info = addIn(currentRoot->next->info, currentRoot->next->next->info);
			delete(currentRoot->next->next);
			delete(currentRoot->next);
			currentRoot->next = NULL;
			Insert(*source, newNode);
		}
	}
}

void createCode(TREE * first, char* currentCode)
{
	strcat(first->code, currentCode);
	if (first->left)
	{
		char *str = (char *)malloc(sizeof(char)*(strlen(currentCode)+1));
		strcpy(str, currentCode);
		char tmp[] = {'0', '\0'}; 
		strcat(str, tmp);
		createCode(first->left, str);
	} 
	if(first->right)
	{
		char *str = (char *)malloc(sizeof(char)*(strlen(currentCode)+1));
		strcpy(str, currentCode);
		char tmp[] = {'1', '\0'};
		strcat(str, tmp);
		createCode(first->right, str);
	}
}

void printTree(TREE * first)
{
	if(first->left)
		printTree(first->left);
	else
		printf("\n%c %1.5f %s", first->info.name, first->info.freq, first->code);
	if(first->right)
		printTree(first->right);
}

char * findCode(characters * root, char a)
{
	while(root->info != a)
		root = root->next;
	return root->code;
}

void createCharacters(characters ** root, characters *newNode)
{
	characters *current = *root;
	if (!current)
	{
		(*root) = newNode;
		return;
	}
	while(current->next)
		current = current->next;
	current->next = newNode;
}

void findInTree(TREE * root, characters ** first)
{
	if(root->left)
		findInTree(root->left, first);
	else
	{
		characters *current = (characters *)malloc(sizeof(characters));
		current->next = NULL;
		current->code = (char *)malloc(sizeof(char)*strlen(root->code));
		current->freq = root->info.freq;
		strcpy(current->code, root->code);
		current->info = root->info.name;
		createCharacters(first, current);
	}
	if(root->right)
		findInTree(root->right, first);
}
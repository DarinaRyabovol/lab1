#include "symbol.h"

#define listOfNodes struct listofnode
#define TREE struct tree
#define characters struct ch

struct tree
{
	Symbol info;
	char *code;
	TREE *left;
	TREE *right;
};

struct listofnode
{
	TREE * info;
	listOfNodes *next;
};

struct ch
{
	char info;
	double freq;
	char *code;
	characters * next;
};

TREE* createNode(Symbol source);

TREE* addIn(TREE * node1, TREE * node2);

void Insert(listOfNodes *first,listOfNodes *newEl);

listOfNodes * createListOfNode(Symbol* arr, int count);

void addInEnd(listOfNodes** root, listOfNodes * newEl);

void bildTree(listOfNodes **source);

void createCode(TREE * first, char *currentCode);

void printTree(TREE * first);

char * findCode(characters * root, char a);

void createCharacters(characters ** root, characters *newNode);

void findInTree(TREE * root, characters ** first);